import React from 'react';
import { css } from 'emotion';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';

const cardStyle = css`
  margin-top: 1rem;
`;
const searchStyle = css`
  position: relative;
  width: 100%;
`;
const searchIconStyle = css`
  height: 100%;
  position: absolute;
  pointer-events: none;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const searchInputStyle = css`
  position: relative;
  border-radius: 4px;
  background-color: #eee;
  margin-left: 25px;
  padding-left: 5px;
  width: 90%;
`;
const searchControlStyle = css`
  text-align: right;
`;

const ProfileSearch = () => (
  <Card className={cardStyle}>
    <CardContent>
      <div className={searchStyle}>
        <div className={searchIconStyle}>
          <SearchIcon />
        </div>
        <InputBase
          placeholder="Search evidence or credits"
          className={searchInputStyle}
        />
      </div>
      <div className={searchControlStyle}>
        <FormControlLabel
          control={<Radio color="primary" />}
          label="Evidence"
          labelPlacement="end"
        />
        <FormControlLabel
          control={<Radio color="primary" />}
          label="Credits"
          labelPlacement="end"
        />
      </div>
    </CardContent>
  </Card>
);

export default ProfileSearch;
