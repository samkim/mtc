import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';
import { injectGlobal } from 'emotion';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import Header from './header';
import ProfileCard from './profile-card';
import ProfileSearch from './profile-search';
import TranscriptView from './transcript-view';
import './layout.css';

// Hook to specify styles with regular CSS
injectGlobal`
  body {
    background-color: #ddd;
  }
`;

// Use theme to set global color palette
const theme = createMuiTheme({
  palette: {
    primary: { main: '#77777A' },
    secondary: { main: '#9F7BDB' },
  },
});

const Content = styled('div')`
  margin: 2rem auto 0 auto;
  max-width: 960px;
`;

// View component defines layout and styling
// Receiving example props as data for now
const StudentView = ({ name, gradDate }) => (
  <MuiThemeProvider theme={theme}>
    <Header />
    <Content>
      <Grid container spacing={24}>
        <Grid item xs={4}>
          <ProfileCard name={name} gradDate={gradDate} />
          <ProfileSearch />
        </Grid>
        <Grid item xs={8}>
          <TranscriptView />
        </Grid>
      </Grid>
    </Content>
  </MuiThemeProvider>
);

StudentView.propTypes = {
  name: PropTypes.node.isRequired,
  gradDate: PropTypes.node.isRequired,
};

export default StudentView;
