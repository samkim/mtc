import React from 'react';
import { css, cx } from 'emotion';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import { Images } from './utility-style';
import profileImg from '../images/profile.jpg';

const cardStyle = css`
  text-align: center;
`;
// Example usage of utility style css
const cardMediaStyle = cx(
  css`
    height: 200px;
    width: 200px;
    margin: 1rem auto;
  `,
  Images.circleMask,
);

const ProfileCard = ({ name, gradDate }) => (
  <Card className={cardStyle}>
    <CardActionArea>
      <CardMedia className={cardMediaStyle} image={profileImg} />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {name}
        </Typography>
        <Typography gutterBottom component="p">
          Graduation: {gradDate}
        </Typography>
        <Typography gutterBottom component="p">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel
          gravida quam. Maecenas elementum nisi id velit auctor, non condimentum
          ipsum posuere.
        </Typography>
      </CardContent>
    </CardActionArea>
  </Card>
);

export default ProfileCard;
