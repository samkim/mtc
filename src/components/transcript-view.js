import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Grid from '@material-ui/core/Grid';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import { css } from 'emotion';

const TranscriptView = () => (
  <>
    <Tabs indicatorColor="primary" textColor="primary" fullWidth>
      <Tab
        label="Student"
        className={css`
          border: 1px solid #eee;
          border-bottom: none;
          background-color: #fff;
        `}
      />
      <Tab
        label="School"
        className={css`
          background-color: #eee;
        `}
      />
    </Tabs>
    <TranscriptSection />
  </>
);

const TranscriptSection = () => (
  <div>
    <FeatureEvidenceSection />
    <AdvancedCreditsSection />
    <ExpansionPanel disabled>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>Courses Taken</Typography>
      </ExpansionPanelSummary>
    </ExpansionPanel>
  </div>
);

const FeatureEvidenceSection = () => (
  <ExpansionPanel>
    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
      <Typography>Featured Evidence</Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
      <Grid container spacing={24}>
        <Grid item xs={6}>
          <Card>
            <CardActionArea>
              <CardMedia
                className={css`
                  height: 200px;
                `}
                image="https://via.placeholder.com/300x200"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Impact Public Schools Makerspace
                </Typography>
                <Typography component="p">
                  Research, design, and build a marketplace
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button size="small" color="primary">
                Read More
              </Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item xs={6}>
          <Card>
            <CardActionArea>
              <CardMedia
                className={css`
                  height: 200px;
                `}
                image="https://via.placeholder.com/300x200"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Problem-finding and Prototyping
                </Typography>
                <Typography component="p">
                  Students can identiy, frame, and thoroughly evaluate a problem
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button size="small" color="primary">
                Read More
              </Button>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    </ExpansionPanelDetails>
  </ExpansionPanel>
);

const creditStyles = css`
  background-color: #eee;
  width: 100%;
  padding: 1em;
  margin-bottom: 0.5rem;
`;
const labelStyle = css`
  text-align: center;
`;
const lineStyle = css`
  border-left: 2px solid #ccc;
  height: 6rem;
  margin-left: 50%;
`;
const Circle = ({ radius }) => (
  <div
    className={css`
      width: 100%;
      text-align: center;
    `}>
    <div
      className={css`
        height: ${radius}px;
        width: ${radius}px;
        background-color: #9f7bdb;
        border-radius: 50%;
        display: inline-block;
      `}>
      {radius}
    </div>
  </div>
);

const AdvancedCreditsSection = () => (
  <ExpansionPanel>
    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
      <Typography>Advanced Credits</Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Typography component="h3" className={creditStyles}>
            Distribution by Credit
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography component="h5" className={labelStyle}>
            Collaboration
          </Typography>
          <div className={lineStyle} />
          <Circle radius={50} />
        </Grid>
        <Grid item xs={3}>
          <Typography component="h5" className={labelStyle}>
            Critical Thinking
          </Typography>
          <div className={lineStyle} />
          <Circle radius={80} />
        </Grid>
        <Grid item xs={3}>
          <Typography component="h5" className={labelStyle}>
            Communication
          </Typography>
          <div className={lineStyle} />
          <Circle radius={40} />
        </Grid>
        <Grid item xs={3}>
          <Typography component="h5" className={labelStyle}>
            Creativity
          </Typography>
          <div className={lineStyle} />
          <Circle radius={60} />
        </Grid>
      </Grid>
    </ExpansionPanelDetails>
  </ExpansionPanel>
);

export default TranscriptView;
