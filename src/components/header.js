import React from 'react';
import { Link } from 'gatsby';
import { css } from 'emotion';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import logo from '../images/gatsby-icon.png';

const logoStyle = css`
  height: 20px;
  margin-bottom: 0;
`;

const Header = () => (
  <AppBar position="static" color="primary">
    <Toolbar>
      <Typography variant="h6">
        <Link to="/">
          <img src={logo} alt="Logo" className={logoStyle} />
        </Link>
      </Typography>
    </Toolbar>
  </AppBar>
);

export default Header;
