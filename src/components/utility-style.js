import { cx, css } from 'emotion';

const Typography = {
  base: css`
    font-family: Roboto;
    font-style: normal;
  `,
};
Typography.primary = cx(
  Typography.base,
  css`
    color: #000;
  `,
);
Typography.secondary = cx(
  Typography.base,
  css`
    color: #333;
    letter-spacing: 0.01em;
    text-transform: uppercase;
  `,
);

const Images = {
  circleMask: css`
    border-radius: 50%;
  `,
};

export { Images, Typography };
