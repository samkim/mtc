import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import Helmet from 'react-helmet';

import StudentView from '../components/student-view';

// Data component
// Sample query to get basic, static site data for now
const IndexPage = () => (
  <StaticQuery
    query={graphql`
      query StudentQuery {
        site {
          siteMetadata {
            title
            student {
              name
              gradDate
            }
          }
        }
      }
    `}
    render={(data) => (
      <>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            { name: 'description', content: 'Sample' },
            { name: 'keywords', content: 'sample, something' },
          ]}>
          <html lang="en" />
        </Helmet>
        <StudentView {...data.site.siteMetadata.student} />
      </>
    )}
  />
);

export default IndexPage;
